const express = require('express')
const path = require('path')
const randomId = require('random-id')
const app = express(),
      bodyParser = require("body-parser")
      port = 3080
const favLists = []

app.use(bodyParser.json())
app.use(express.static(path.join(__dirname, '../vue-app/dist')))

app.get('/api/favlists', (req, res) => {
  res.json(favLists)
})

app.post('/api/favlists', (req, res) => {
  const data = req.body
  console.log(data)
  favLists.push({
    name: data.alias,
    list: data.list
  })
  res.json("Favorite list added")
})

app.get('/api/favlistcount', (req, res) => {
  res.json(favLists.length)
})

app.get('/', (req,res) => {
  res.sendFile(path.join(__dirname, '../vue-app/build/index.html'))
})

app.listen(port, () => {
    console.log(`Server listening on the port ${port}`)
})
