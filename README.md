# ALTEN @ Fullstack test - Vue/NodeJS

### Requisitos
- Tener instalado ``docker-compose`` en nuestra máquina.
- Disponer de los puertos `8080` y `3080` libres.

### Instalación
1. Posicionados en la raíz del proyecto ejecutamos ``docker-compose up --build``.
2. Una vez se complete la creación de los contenedores accedemos a la url ``localhost:8080``.

### Descripción
Tras leer los requisitos he decidido utilizar ``docker-compose`` por comodidad. Los 2 proyectos (la parte front en Vue
y el back en express) corren en 2 contenedores distintos y se muestran como 2 proyectos separados, por lo que están
desacoplados y se podrían tener sin problema en 2 repos distintos.

En el fichero ``docker-compose.yml`` se describen los 2 componentes a crear y sus instrucciones en cada ``Dockerfile``.

La aplicación consta de un front en Vue que se alimenta del API de https://randomuser.me y un microservicio escrito en 
NodeJS (express) que actúa como backend para gestionar las listas de usuarios guardadas como favoritas.

**NodeJS Backend**: Es la parte más sencilla, consta de un simple fichero principal ``server.js`` que actúa como
API/backend. Tal y como se especifica los datos se guardan en memoria y se pierden al reiniciar el servidor.

**Vue APP**: La parte front está hecha en Vue. Los componentes principales son:
- **Dashboard**: Es el componente principal, el listado donde se obtienen los usuarios al pulsar sobre "Consultar perfiles",
a su vez contiene 3 subcomponentes:
  - Filters: Componente en el que se gestiona el filtrado/paginado. Emite un string formateado con los filtros utilizando
  la prop ``filterStr``.
  - DisplayBoard: Componente dedicado a mostrar datos extra sobre las listas, actualmente muestra el nº de listas que
  contiene nuestro backend.
  - Profiles: Se encarga de mostrar la tabla de resultados con su paginación.
- **ProfileDetail**: Es la segunda vista en la que podemos ver la información detallada de cada Usuario. Contiene a su vez
el componente FavList que se muestra cuando hemos añadido al menos un perfil a la lista de favoritos. Esta lista se va
llenando a la vez que navegamos por los distintos perfiles y los marcamos como favoritos. Una vez que tenemos la lista
con los usuarios que deseamos, podemos escribir un alias y persistirla.
- **Favorites**: Aquí veremos las listas que hemos guardado en el backend junto al botón de exportar a csv. 

### Apuntes
Dejo un par de apuntes que he hecho como mejoras/tips:
- Se ha añadido una configuración como ``manifest.json`` y un worker ficticio para cumplir el estándar de PWA.
- Añadido `vue-router` para disponer de rutas y a su vez convertir la app en una SPA con una agradable transición.
- En principio comencé el desarrollo sin recurrir a ``Vuex``, sin embargo lo terminé añadiendo por necesidad en
poder comunicarme con la creación de la lista y el usuario seleccionado.
- Se han añadido algunas librerías de apoyo (moment para las fechas, linter para cuidar el code, etc.).

Por otro lado comentar que el paginado es "ficticio" ya que desconozco como obtener un "total real" de rows en la API
de RandomUser. Lo mismo me ocurre para filtrar por fecha de nacimiento, de lo que he buscado incluso en su repo y creo
que no es posible https://github.com/RandomAPI/Randomuser.me-Node/issues/116.

Me habría gustado tener más tiempo y añadir tests 🧪...


