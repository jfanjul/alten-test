import Vue from 'vue'
import App from './App.vue'

import router from './router'
import store from './store'

import './assets/styles/lib/bulmamaterial.min.css'

Vue.config.productionTip = false

router.afterEach((to) => {
  // Add a body class specific to the route we're viewing
  var el = document.querySelector('[class*="alten-app-page--"]')
  for (let i = el.classList.length - 1; i >= 0; i--) {
    const className = el.classList[i]
    if (className.startsWith('ativo')) {
      el.classList.remove(className)
    }
  }
  document.querySelector('body').classList.add('alten-app-page--' + to.name)
})

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
