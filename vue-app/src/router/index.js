import Vue from 'vue'
import Router from 'vue-router'

// Components
import Dashboard from "@/components/Dashboard"
import ProfileDetail from "@/components/ProfileDetail"
import Favorites from "@/components/Favorites"

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      alias: '/dashboard',
      name: 'Dashboard',
      component: Dashboard
    },
    {
      path: '/profile',
      name: 'ProfileDetail',
      component: ProfileDetail
    },
    {
      path: '/favorites',
      name: 'Favorites',
      component: Favorites
    },
  ],
  mode: 'history',
  base: '/',

  // Prevents window from scrolling back to top
  // when navigating between components/views
  scrollBehavior (to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

export default router

router.beforeEach((to, from, next) => {
  next()
})
