const RANDOM_USERS_API_BASE = 'https://randomuser.me/api/'

export async function getRandomUsers(filters = '') {
  const response = await fetch(`${RANDOM_USERS_API_BASE}${filters}`);
  return await response.json();
}
