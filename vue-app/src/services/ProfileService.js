export async function getAllFavLists() {
  const response = await fetch('/api/favlists');
  return await response.json();
}
export async function getAllFavListsCount() {
  const response = await fetch('/api/favlistcount');
  return await response.json();
}

export async function storeFavList(data) {
  const response = await fetch(`/api/favlists`, {
    method: 'POST',
    headers: {'Content-Type': 'application/json'},
    body: JSON.stringify(data)
  })
  return await response.json();
}
