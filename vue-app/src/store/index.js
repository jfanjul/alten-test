import Vue from 'vue'
import Vuex from 'vuex'
import { storeFavList, getAllFavListsCount } from '../services/ProfileService'

Vue.use(Vuex)

const state = {
  currentProfile: null,
  favList: [],
  favListsCount: 0
}

const getters = {
  currentProfile: state => state.currentProfile,
  favList: state => state.favList,
  hasInFavList: state => state.favList.find(element => element.email === state.currentProfile.email),
  favListsCount: state => state.favListsCount
}

const actions = {
  selectCurrentProfile({ commit }, data) {
    commit('SELECT_PROFILE', { profile: data })
  },
  addFavProfile({ commit }, data) {
    commit('ADD_FAV_PROFILE', { profile: data })
  },
  removeFavProfile({ commit }, data) {
    commit('REMOVE_FAV_PROFILE', { profile: data })
  },
  storeCurrentFavList({ commit, dispatch }, data) {
    storeFavList({ alias: data, list: state.favList }).then(() => {
      commit('ERASE_FAV_LIST')
      dispatch('getFavListCount')
    })
  },
  getFavListCount({ commit }) {
    getAllFavListsCount().then(count => {
      commit('SET_FAV_LIST_COUNT', count)
    })
  },
}

const mutations = {
  ['SELECT_PROFILE'] (state, data) {
    state.currentProfile = data.profile
  },
  ['ADD_FAV_PROFILE'] (state, data) {
    state.favList.push(data.profile)
  },
  ['REMOVE_FAV_PROFILE'] (state) {
    const filtered = state.favList.filter(element => element.email !== state.currentProfile.email)
    state.favList = filtered
  },
  ['ERASE_FAV_LIST'] (state) {
    state.favList = []
  },
  ['SET_FAV_LIST_COUNT'] (state, data) {
    state.favListsCount = data
  },
}

export default new Vuex.Store({
  state,
  getters,
  actions,
  mutations
})
